// Create the configuration
// http://davidwalsh.name/nodejs-irc


var config = {

    channels: ["#falkens-labor"],
    server: "irc.hackint.org",
    botName: "WOPR",
    commandChar: '?'
};

// Get the lib
//noinspection JSUnresolvedFunction
var irc = require("irc");

// Create the bot name
var bot = new irc.Client(config.server, config.botName, {
    userName: 'wopr',
    realName: 'W.O.P.R',
    autoRejoin: true,
    autoConnect: true,
    channels: config.channels,
    floodProtection: true,
    floodProtectionDelay: 1000
});

// Load language file
//noinspection JSUnresolvedFunction
var lang = require('./modules/lang.js');

// Load user module
//noinspection JSUnresolvedFunction
var user = require('./modules/user.js');


bot.addListener("message", function (from, to, text, message) {

    console.log(text);
    console.log(text[0]);
    if (text[0] == config.commandChar) {
        console.log('Command Detected!');
        var args = text.split(" ");
        console.log(args);
        var command = args[0].substr(1, args[0].length);
        if (typeof command == "string") {
            switch (command) {
                case 'die':
                    if (user.isAdmin(from)) {
                        bot.say(to, "Executing Seppuku ...");
                        setTimeout(function () {
                            //noinspection JSUnresolvedVariable
                            process.exit(0)
                        }, 3000);

                    } else {
                        bot.say(to, "WTF?!");
                    }
                    break;
                case 'join':
                    if (user.isAdmin(from)) {
                        if (typeof args[1] == "string") {
                            bot.say(to, "Joining channel ...");
                            bot.join(args[1]);
                        } else {
                            bot.say(to, lang.parameterMissing);
                        }

                    } else {
                        bot.say(to, lang.notAdmin);
                    }
                    break;

                case 'games':
                    if (user.isAdmin(from)) {
                        if (typeof args[1] == "string") {
                        } else {
                            bot.say(to, "## Games Available:");
                            bot.say(to, "1 - Chess");
                            bot.say(to, "2 - Poker");
                        }

                    } else {
                        bot.say(to, lang.notAdmin);
                    }
                    break;
                case 'user':
                    if (typeof args[1] == "string") {
                        switch (args[1]) {
                            case 'status':
                                if (typeof args[2] == "string") {
                                    var argUser = args[2];
                                } else {
                                    var argUser = from;
                                }
                                bot.say(to, "## Status of User '" + argUser + "'");
                                if (user.isAdmin(from)) {
                                    bot.say(to, "User is Admin!");
                                } else {
                                    bot.say(to, "User is unknown!")
                                }
                                break;
                        }
                    } else {
                        bot.say(to, "## User Commands:");
                        bot.say(to, "# status [nick] - Status of user");
                    }
                    break;
            }
        }
    } else {
        console.log('Text is noc command!');
    }
});

