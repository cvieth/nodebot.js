/**
 * Created with JetBrains PhpStorm.
 * User: cvieth
 * Date: 20.09.13
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */

var adminUsers = ['prof-falken'];

/**
 * Checks if given Nickname is a Admin user
 *
 * @param nick
 * @returns {boolean}
 */
exports.isAdmin = function (nick) {
    return adminUsers.indexOf(nick) >= 0;
};
