/**
 * Created with JetBrains PhpStorm.
 * User: cvieth
 * Date: 20.09.13
 * Time: 23:28
 * To change this template use File | Settings | File Templates.
 */

exports.notAdmin = "You are not allowed to execute this command!";
exports.parameterMissing = "Some parameters are missing!";
