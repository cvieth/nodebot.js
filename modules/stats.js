/**
 * Created with JetBrains PhpStorm.
 * User: cvieth
 * Date: 20.09.13
 * Time: 22:48
 * To change this template use File | Settings | File Templates.
 */

//Counters
var counter = {
    join: 0,
    part: 0,
    msg: 0
}

exports.init = function (bot) {
    bot.addListener("join", function (channel, who) {
        counter.join = counter.join + 1;
    });

    bot.addListener("part", function (channel, nick, reason, message) {
        counter.join = counter.part + 1;
    });

    bot.addListener("message#", function (nick, to, text, message) {
        counter.msg = counter.msg + 1;
    });

    bot.addListener("message#", function (from, to, text, message) {
        if (text == "!stats") {
            bot.say(to, "### Statistics ###");
            bot.say(to, "# Joins: " + counter.join);
            bot.say(to, "# Parts: " + counter.part);
            bot.say(to, "# Messages: " + counter.msg);
        }
    });
}
